<?php

namespace App\Controller\Admin;

use App\Controller\QuestionController;
use App\Controller\VoteController;
use App\Entity\Question;
use App\Entity\User;
use App\Entity\Vote;
use EasyCorp\Bundle\EasyAdminBundle\Command\MakeAdminDashboardCommand;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use http\Url;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        $routeBuilder = $this->get(CrudUrlGenerator::class)->build();

        return $this->redirect($routeBuilder->setController(UserCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Administration de Sondage créateur');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToCrud('User', 'fa fa-user', User::class);
        yield MenuItem::linkToCrud('Question', 'fa fa-question', Question::class);
        yield MenuItem::linkToCrud('Votes', 'fa fa-poll', Vote::class);
        yield MenuItem::linkToUrl('Mon profil', 'fa fa-file-text', 'profil');
        yield MenuItem::linkToLogout('Se déconnecter', 'fa fa-sign-out');

    }
}
