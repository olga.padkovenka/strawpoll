<?php

namespace App\Controller;

use App\Entity\Question;
use App\Entity\User;
use App\Entity\Vote;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAccountStatusException;

class VoteController extends AbstractController
{
    /**
     * @Route("/vote/{id}", name="vote_get", methods={"GET"})
     * @param int $id
     * @param response
     * @return Response
     */
    public function index(int $id): Response
    {
        $question = $this->getDoctrine()->getRepository(Question::class)->find($id);
        return $this->render('vote/index.html.twig', [
            'controller_name' => 'VoteController',
            'question' => $question
        ]);
    }

    /**
     * @Route("/vote/{id}", name="vote_post", methods={"POST"})
     * @param int $id
     * @param Request $request
     * @return Response
     */
    public function vote (int $id, Request $request): Response
    {
        $question = $this->getDoctrine()->getRepository(Question::class)->find($id);

        $json = json_decode($request->getContent(), true);


       if (isset($json["vote"]))
       {

           $myIp = $request->getClientIp(); //je récupère l'ip de l'utilisateur
           $entityManager = $this->getDoctrine()->getManager();

           // check si l'utilisateur à déjà voté
           $votes = $question->getVotes()->toArray(); //Je récupère les utilisateurs déjà votés, je transforme en tableau
           $foundVote = array_filter($votes, function (Vote $v) use ($myIp) { // je fais la boucle
               return $myIp === $v->getIp();
           });
           if (count($foundVote) > 0)

               return $this->json(["error" => "Déjà voté"], Response::HTTP_INTERNAL_SERVER_ERROR);

           $reponse = $question->getReponses()->get($json["vote"]);

           $vote = new Vote();
           $vote->setQuestionId($question);
           $vote->setIp($myIp); //j'enregistre l'ip.
           $vote->setReponseId($reponse);
           $question->addVote($vote);
           $reponse->addVote($vote);

           $entityManager->persist($vote);
           $entityManager->persist($question);
           $entityManager->persist($reponse);
           $entityManager->flush();

           return new Response();
       }
       return new Response("Error", Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
