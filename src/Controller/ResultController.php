<?php

namespace App\Controller;

use App\Entity\Question;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ResultController extends AbstractController
{
    /**
     * @Route("/result/{id}", name="result")
     * @param int $id
     * @return Response
     */
    public function index(int $id): Response
    {
        $question = $this->getDoctrine()->getRepository(Question::class)->find($id);
        return $this->render('result/index.html.twig', [
            'controller_name' => 'ResultController',
            'question' => $question,
        ]);
    }

}
