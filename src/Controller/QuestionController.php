<?php

namespace App\Controller;

use App\Entity\Question;
use App\Entity\Reponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class QuestionController extends AbstractController
{
    /**
     * @Route("/create", name="create")
     * @param Request $request
     * @return Response
     */
    public function create(Request $request): Response
    {
        $json = json_decode($request ->getContent(), true);


        if (isset($json["question"]) && isset($json["reponses"]))
        {
            $entityManager = $this->getDoctrine()->getManager(); //Pour sauvegarder l'info dans la base de données et l'extraire
            $question = new Question();
            $question->setName($json["question"]);

            $question->setOwner($this->getUser());


            foreach ($json["reponses"] as $value)
            {
                if ($value)
                {
                    $reponse = new Reponse();
                    $reponse->setName($value);
                    $reponse->setQuestion($question);
                    $question->addReponse($reponse);

                    $entityManager->persist($reponse);

                }

                $entityManager->persist($question);
                $entityManager->flush();
            }


            return $this->json([
                "id" => $question->getId()
            ]);
        }
        return new Response("Error", Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @Route("/question_delete/{id}", name="question_delete")
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function question_delete (Request $request, int $id):Response
    {
        $entityManager = $this->getDoctrine()->getManager(); //je récupère l'entity manager

        //Je récupère le question
        $question = $this->getDoctrine()->getRepository(Question::class)->find($id);
            $entityManager->remove($question);
            $entityManager->flush();

        return $this->json([
            "id" => $question->getId()
        ]);

    }
    /**
     * @Route("/question", name="question")
     */
    public function index(): Response
    {
        return $this->render('question/index.html.twig', [
            'controller_name' => 'QuestionController',
        ]);
    }

}
