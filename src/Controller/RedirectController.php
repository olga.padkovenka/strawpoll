<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RedirectController extends AbstractController
{
    /**
     * @Route("/redirect", name="redirect")
     */
    public function index(): Response
    {
      if(in_array('ROLE_ADMIN',
      $this->getUser()->getRoles())) {
          return $this->redirectToRoute('admin');
      }
      return $this->redirectToRoute('profil');
    }
}
